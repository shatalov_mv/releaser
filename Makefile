MODULE=gitlab.com/sprinttechnologies/libs/golibs/magnit/v2
PACKAGES=version tracing sentry met logger healthcheck grpc/interceptor \
echomagnit/debug echomagnit/debug/pprof echomagnit/debug/view echomagnit/middleware echomagnit/openapi echomagnit/render authorization
PACKAGES_WITH_EXISTING_MD_README=metrics rmq rs pgtracer

VERSION=999.0.0
CLI_EXECUTOR_ADDRESS_DEV=cli-executor-grpc.k8s.dev.platform.corp:443
CLI_EXECUTOR_ADDRESS_LOCAL=localhost:8002

CI_ADMIN_ADDRESS_DEV=https://ci-admin.k8s.dev.platform.corp
CI_ADMIN_ADDRESS_LOCAL=http://localhost:8000

.PHONY: gen-doc-for-runtime build-local-docker build-local-cmd test-compilation

# Generate documentation
gen-doc-for-runtime:
	@echo "generating README.md files for packages:" $(PACKAGES) $(PACKAGES_WITH_EXISTING_MD_README)

	@for ppp in $(PACKAGES); do \
	  gomarkdoc --repository.default-branch master --output runtime/$$ppp/README.md $(MODULE)/runtime/$$ppp/; \
	done

	@for ppp in $(PACKAGES_WITH_EXISTING_MD_README); do \
    	  gomarkdoc -e --repository.default-branch master --output runtime/$$ppp/README.md $(MODULE)/runtime/$$ppp/; \
    done


go-generate:
	go install github.com/abice/go-enum@latest; \
	cd cmd/magnit; \
	go generate ./...


# Компиляция cmd для использования локальных Dockerfile's
build-local-cmd:
	cd cmd/magnit && \
 	go build \
 	-ldflags="-X 'gitlab.com/sprinttechnologies/libs/golibs/magnit/cmd/magnit/pkg/version.Version=$(VERSION)' \
 	-X 'gitlab.com/sprinttechnologies/libs/golibs/magnit/cmd/magnit/pkg/docker.SkipPull=true' \
 	-X 'gitlab.com/sprinttechnologies/libs/golibs/magnit/cmd/magnit/pkg/cliexecutor.Address=$(CLI_EXECUTOR_ADDRESS_LOCAL)' \
 	-X 'gitlab.com/sprinttechnologies/libs/golibs/magnit/cmd/magnit/pkg/ciadmin.Address=$(CI_ADMIN_ADDRESS_LOCAL)'" \
 	-o ../../bin/magnit

build-dev-cmd:
	cd cmd/magnit && \
 	go build \
 	-ldflags="-X 'gitlab.com/sprinttechnologies/libs/golibs/magnit/cmd/magnit/pkg/version.Version=$(VERSION)' \
 	-X 'gitlab.com/sprinttechnologies/libs/golibs/magnit/cmd/magnit/pkg/docker.SkipPull=true' \
 	-X 'gitlab.com/sprinttechnologies/libs/golibs/magnit/cmd/magnit/pkg/cliexecutor.Address=$(CLI_EXECUTOR_ADDRESS_DEV)' \
 	-X 'gitlab.com/sprinttechnologies/libs/golibs/magnit/cmd/magnit/pkg/ciadmin.Address=$(CI_ADMIN_ADDRESS_DEV)'" \
 	-o ../../bin/magnit

test-compilation: build-dev-cmd
	cd tests; \
	./../bin/magnit codegen update --no-lint; \
	cd compiled-tests; \
	go test ./...

test-integration: build-dev-cmd
	cd tests; \
	./../bin/magnit codegen update --no-lint; \
	cd integration-tests; \
	go test -coverpkg=../gen/... -coverprofile=coverage.txt ./...; \
	go tool cover -func coverage.txt | grep total | awk '{print "coverage:/ "$3}'; \
	rm coverage.txt
