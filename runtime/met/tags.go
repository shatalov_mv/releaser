package met

import "github.com/smira/go-statsd"

const (
	domainTag = "domain"
	actionTag = "action"
	resultTag = "result"
)

type Tag struct {
	K, V string
}

func T(k, v string) Tag {
	return Tag{K: k, V: v}
}

func Reason(v string) Tag {
	return Tag{K: "reason", V: v}
}

func formatTags(domain, action, result string, customTags []Tag) []statsd.Tag {
	tags := make([]statsd.Tag, 0, len(customTags)+3) //nolint:gomnd // заранее известное число тегов по умолчанию

	for _, t := range customTags {
		tags = append(tags, statsd.StringTag(t.K, t.V))
	}

	tags = append(
		tags,
		statsd.StringTag(domainTag, domain),
		statsd.StringTag(actionTag, action),
		statsd.StringTag(resultTag, result),
	)

	return tags
}
