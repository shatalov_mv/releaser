// Package met предоставляет функциональность для отправки различных метрик, используя клиент StatsD.
package met

import (
	"errors"
	"fmt"
	"time"

	"github.com/smira/go-statsd"
	"go.uber.org/zap"
)

// Client - структура клиента для отправки метрик через StatsD.
type Client struct {
	innerClient *statsd.Client
}

// Opt определяет настройки клиента для отправки метрик.
type Opt struct {
	NoOp       bool   // Если true, клиент будет работать в режиме "не делать ничего" (не отправлять метрики).
	MetricName string // Префикс, добавляемый к именам метрик.
	Address    string // Адрес сервера StatsD для отправки метрик.
	Port       string // Порт сервера StatsD для отправки метрик.

	Logger *zap.Logger // Экземпляр логгера для логирования событий клиента.
}

// statsDLogger предоставляет собственный логгер для интеграции с go-statsd.
type statsDLogger struct {
	*zap.Logger
}

// Printf реализует интерфейс Printf для statsDLogger.
func (n *statsDLogger) Printf(fmtString string, args ...any) {
	n.Error(fmt.Sprintf(fmtString, args...))
}

// globalClient - глобальный клиент для отправки метрик.
var globalClient *Client

// Init инициализирует глобальный клиент для отправки метрик с заданными параметрами (opt).
func Init(opt Opt) error {
	if opt.NoOp {
		client := Client{nil}
		globalClient = &client

		return nil
	}

	if opt.Logger == nil {
		return errors.New("opt.Logger is nil")
	}

	c := statsd.NewClient(fmt.Sprintf("%s:%s", opt.Address, opt.Port),
		statsd.MetricPrefix(opt.MetricName),
		statsd.TagStyle(statsd.TagFormatInfluxDB),
		statsd.ReconnectInterval(5*time.Second), //nolint:gomnd // будем подбирать экспериментальным путем
		statsd.Logger(&statsDLogger{opt.Logger}),
	)

	client := Client{innerClient: c}

	globalClient = &client

	return nil
}

// Close закрывает глобальный клиент для отправки метрик.
func Close() {
	if isNil() {
		return
	}

	_ = globalClient.innerClient.Close() // клиент всегда возвращает nil
}

// isNil это проверка для любых операций с *Client для реализации no-op подхода
func isNil() bool {
	return globalClient == nil || globalClient.innerClient == nil
}
