package met

import (
	"time"

	"github.com/smira/go-statsd"
)

// Incr увеличивает счетчик для заданных domain, action и result на указанное количество (count) и добавляет дополнительные теги (tag).
func Incr(domain, action, result string, count int64, tag ...Tag) {
	if isNil() {
		return
	}

	globalClient.innerClient.Incr(
		"",
		count,
		formatTags(domain, action, result, tag)...,
	)
}

// IncrAny увеличивает счетчик для заданного имени (name) на указанное количество (count) и добавляет дополнительные теги (tag).
func IncrAny(name string, count int64, tag ...Tag) {
	if isNil() {
		return
	}

	tags := make([]statsd.Tag, 0, len(tag))

	for _, t := range tag {
		tags = append(tags, statsd.StringTag(t.K, t.V))
	}

	globalClient.innerClient.Incr(
		name,
		count,
		tags...,
	)
}

// IncrOne увеличивает счетчик для заданных domain, action и result на 1 и добавляет дополнительные теги (tag).
func IncrOne(domain, action, result string, tag ...Tag) {
	Incr(domain, action, result, 1, tag...)
}

// Decr уменьшает счетчик для заданных domain, action и result на указанное количество (count) и добавляет дополнительные теги (tag).
func Decr(domain, action, result string, count int64, tag ...Tag) {
	if isNil() {
		return
	}

	globalClient.innerClient.Decr(
		"",
		count,
		formatTags(domain, action, result, tag)...,
	)
}

// DecrOne уменьшает счетчик для заданных domain, action и result на 1 и добавляет дополнительные теги (tag).
func DecrOne(domain, action, result string, tag ...Tag) {
	Decr(domain, action, result, 1, tag...)
}

// DecrAny уменьшает счетчик для заданного имени (name) на указанное количество (count) и добавляет дополнительные теги (tag).
func DecrAny(name string, count int64, tag ...Tag) {
	if isNil() {
		return
	}

	tags := make([]statsd.Tag, 0, len(tag))

	for _, t := range tag {
		tags = append(tags, statsd.StringTag(t.K, t.V))
	}

	globalClient.innerClient.Decr(
		name,
		count,
		tags...,
	)
}

// Timing отправляет timing-метрику с заданными domain, action, result и продолжительностью (delta) и добавляет дополнительные теги (tag).
func Timing(domain, action, result string, delta time.Duration, tag ...Tag) {
	if isNil() {
		return
	}

	globalClient.innerClient.PrecisionTiming(
		"",
		delta,
		formatTags(domain, action, result, tag)...,
	)
}

// Gauge устанавливает значение gauge-метрики для заданных domain, action, result и значения (value) и добавляет дополнительные теги (tag).
func Gauge(domain, action, result string, value int64, tag ...Tag) {
	if isNil() {
		return
	}

	globalClient.innerClient.Gauge(
		"",
		value,
		formatTags(domain, action, result, tag)...,
	)
}

// GaugeDelta изменяет значение гейдж-метрики для заданных domain, action, result на указанное значение (value) и добавляет дополнительные теги (tag).
func GaugeDelta(domain, action, result string, value int64, tag ...Tag) {
	if isNil() {
		return
	}

	globalClient.innerClient.GaugeDelta(
		"",
		value,
		formatTags(domain, action, result, tag)...,
	)
}

// SetAdd добавляет значение (value) во множество для заданных domain, action и result и добавляет дополнительные теги (tag).
func SetAdd(domain, action, result, value string, tag ...Tag) {
	if isNil() {
		return
	}

	globalClient.innerClient.SetAdd(
		"",
		value,
		formatTags(domain, action, result, tag)...,
	)
}
