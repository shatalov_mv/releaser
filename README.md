# Magnit

### Установка cli
`go install gitlab.com/sprinttechnologies/libs/golibs/magnit/cmd/magnit@latest`


### Документация cli
- Запуск cli после установки с флагом `--help` содержит все описания для разработчика
- Документация для codegen - [cmd/magnit/codegen/README.md](cmd/magnit/codegen/README.md)
- Документация для lint - [cmd/magnit/linter/README.md](cmd/magnit/linter/README.md)


### Документация зависимостей (runtime)
- authorization - [runtime/authorization/README.md](runtime/authorization/README.md)
- echomagnit
  - echomagnit/debug/pprof [runtime/echomagnit/debug/pprof/README.md](runtime/echomagnit/debug/pprof/README.md)
  - echomagnit/debug/view - [runtime/echomagnit/debug/view/README.md](runtime/echomagnit/debug/view/README.md)
  - echomagnit/debug - [runtime/echomagnit/debug/README.md](runtime/echomagnit/debug/README.md)
  - echomagnit/middleware - [runtime/echomagnit/middleware/README.md](runtime/echomagnit/middleware/README.md)
  - echomagnit/openapi - [runtime/echomagnit/openapi/README.md](runtime/echomagnit/openapi/README.md)
  - echomagnit/render - [runtime/echomagnit/render/README.md](runtime/echomagnit/render/README.md)
- grpc
  - grpc/interceptor - [runtime/grpc/interceptor/README.md](runtime/grpc/interceptor/README.md)
- healthcheck = [runtime/healthcheck/README.md](runtime/healthcheck/README.md) 
- logger - [runtime/logger/README.md](runtime/logger/README.md)
- met - [runtime/met/README.md](runtime/met/README.md)
- metrics - [runtime/metrics/README.md](runtime/metrics/README.md)
- pgtracer - [runtime/pgtracer/README.md](runtime/pgtracer/README.md)
- rmq - [runtime/rmq/README.md](runtime/rmq/README.md)
- rs - [runtime/rs/README.md](runtime/rs/README.md)
- sentry - [runtime/sentry/README.md](runtime/sentry/README.md)
- tracing - [runtime/tracing/README.md](runtime/tracing/README.md)
- version - [runtime/version/README.md](runtime/version/README.md)
