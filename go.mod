module gitlab.com/shatalov_mv/releaser/v2

go 1.20

require (
	github.com/smira/go-statsd v1.3.2
	go.uber.org/zap v1.26.0
)

require (
	github.com/stretchr/testify v1.8.4 // indirect
	go.uber.org/goleak v1.2.1 // indirect
	go.uber.org/multierr v1.11.0 // indirect
)
